{
    "id": "8f3c3ba8-a68c-4e5e-b4fc-c4a14c6e6e19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transmission",
    "eventList": [
        {
            "id": "0e540fba-23f5-4ecc-b855-ddd927a4e50a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f3c3ba8-a68c-4e5e-b4fc-c4a14c6e6e19"
        },
        {
            "id": "49e4efd3-b342-4564-b2b5-f3c8b0431e37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f3c3ba8-a68c-4e5e-b4fc-c4a14c6e6e19"
        },
        {
            "id": "c88dce73-38a3-40fa-bbaf-b1f59d863273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "8f3c3ba8-a68c-4e5e-b4fc-c4a14c6e6e19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}