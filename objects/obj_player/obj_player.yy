{
    "id": "dac5c398-9238-457e-bef7-898c89caf210",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "319a4c2d-0add-4c9f-be2f-ac71e65c1df0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dac5c398-9238-457e-bef7-898c89caf210"
        },
        {
            "id": "f2970db2-60a3-46e1-9b97-ee33f78064cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dac5c398-9238-457e-bef7-898c89caf210"
        },
        {
            "id": "ad9e66d3-f3dd-49ee-b1c8-8a52a8f5e93b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dac5c398-9238-457e-bef7-898c89caf210"
        },
        {
            "id": "44021a9a-3cd9-4403-9292-18067ac36927",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dac5c398-9238-457e-bef7-898c89caf210"
        },
        {
            "id": "26bd999d-2352-4874-b6b6-37d9361fa931",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "dac5c398-9238-457e-bef7-898c89caf210"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "df0105ca-a37b-45ad-a345-f6900333af28",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e3cc3701-19f9-42ac-98c6-617526750cbd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "396e767c-e4a9-4e90-a9d3-466c194e315c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "5f1a2172-999e-42f7-aaae-67c986fbe1ce",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df0161f4-e5dd-4fc4-b7ad-c28c57b9c366",
    "visible": true
}