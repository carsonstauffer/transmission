/// @description Insert description here
// You can write your code in this editor

var impulse = 8
var close_enough = 1

if(target_x >= 0 && target_y >= 0){
	if(point_distance(phy_position_x,phy_position_y,target_x, target_y) <= close_enough){
		target_x = -1
		target_y = -1
		phy_speed_x = 0
		phy_speed_y = 0
	}
	else{
		//move_towards_point(target_x, target_y, movespeed)	
		var dir = point_direction(phy_position_x, phy_position_y, target_x, target_y)
		var impulse_x = lengthdir_x(impulse, dir)
		var impulse_y = lengthdir_y(impulse, dir)
		physics_apply_impulse(phy_position_x, phy_position_y, impulse_x, impulse_y)	
	}
		
}

if(place_meeting(phy_position_x, phy_position_y, obj_player)){
	with(obj_player){
		if(!alarm[1]){
			alarm[1] = 4	
		}
	}
}