{
    "id": "876b6ab3-68d5-48bc-a3b3-3b956ce03152",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "09967cc1-7ca7-4a8d-abd2-d52bb611e9f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "876b6ab3-68d5-48bc-a3b3-3b956ce03152"
        },
        {
            "id": "6d11b81f-7afb-4bf6-bb70-af990b16f86b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "876b6ab3-68d5-48bc-a3b3-3b956ce03152"
        },
        {
            "id": "d1a671b5-801b-4938-a8ef-3c9979209d24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "876b6ab3-68d5-48bc-a3b3-3b956ce03152",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "876b6ab3-68d5-48bc-a3b3-3b956ce03152"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.5,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "db845928-978f-4978-85f2-85def201173a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "c1f0b271-bff1-4ee3-97f6-4889632445bc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 23,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3718cfe8-45ca-4fd7-b981-ed9f1af7ff64",
    "visible": true
}