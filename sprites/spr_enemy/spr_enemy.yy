{
    "id": "3718cfe8-45ca-4fd7-b981-ed9f1af7ff64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f0075db-07a0-4009-887c-2d2507dca64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3718cfe8-45ca-4fd7-b981-ed9f1af7ff64",
            "compositeImage": {
                "id": "c4147e69-9485-492c-b98f-b18fecc27a55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f0075db-07a0-4009-887c-2d2507dca64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6a9a54-8d74-4415-b707-ce5d750b8e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f0075db-07a0-4009-887c-2d2507dca64b",
                    "LayerId": "703899c5-2914-437b-8c47-44903b6ad4ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "703899c5-2914-437b-8c47-44903b6ad4ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3718cfe8-45ca-4fd7-b981-ed9f1af7ff64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}