{
    "id": "df0161f4-e5dd-4fc4-b7ad-c28c57b9c366",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce4d6125-74f6-4785-8ccd-c542b72e2e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df0161f4-e5dd-4fc4-b7ad-c28c57b9c366",
            "compositeImage": {
                "id": "f4d1b73e-2275-4e0a-922f-317bd7bef917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce4d6125-74f6-4785-8ccd-c542b72e2e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb5dd82-fade-48a1-a80c-67143a237c2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce4d6125-74f6-4785-8ccd-c542b72e2e4a",
                    "LayerId": "71cc7e10-6397-4356-b79a-b3f898a18e59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "71cc7e10-6397-4356-b79a-b3f898a18e59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df0161f4-e5dd-4fc4-b7ad-c28c57b9c366",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}